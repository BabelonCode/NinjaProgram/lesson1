﻿using System;

namespace _005_Enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = new ListNode(10);
            root.Add(20);
            root.Add(1000);
            root.Add(40);
            root.Add(50);

            int max = root.value;

            var node = root.next;
            while (node != null)
            {
                if (node.value > max)
                    max = node.value;

                node = node.next;
            }

            Console.ReadLine();
        }
    }
}
