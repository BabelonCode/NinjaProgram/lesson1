﻿namespace _005_Enumerable
{
    class ListNode
    {
        public ListNode(int item)
        {
            value = item;
        }

        public int value;
        public ListNode next;
        private ListNode _cuurentNode;

        public void Add(int item)
        {
            if (_cuurentNode == null)
            {
                next = new ListNode(item);
                _cuurentNode = next;
            }
            else
            {
                _cuurentNode.next = new ListNode(item);
                _cuurentNode = _cuurentNode.next;
            }
        }

        public override string ToString() => value.ToString();
    }
}