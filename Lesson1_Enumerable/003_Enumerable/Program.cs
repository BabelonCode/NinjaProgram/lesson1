﻿namespace _003_Enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            ListNode node1 = new ListNode(10);
            node1
                .Add(20)
                .Add(30)
                .Add(40);
        }
    }
}
