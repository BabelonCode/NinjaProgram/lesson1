﻿namespace _003_Enumerable
{
    class ListNode
    {
        public ListNode(int item)
        {
            value = item;
        }

        public int value;
        public ListNode next;

        public ListNode Add(int item)
        {
            next = new ListNode(item);
            return next;
        }

        public override string ToString() => value.ToString();
    }
}