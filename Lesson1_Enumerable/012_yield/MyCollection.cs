﻿using System.Collections.Generic;

namespace _012_yield
{
    static class MyCollection
    {
        public static IEnumerable<string> AsEnumerable()
        {
            yield return "Hello";
            yield return "Hello";
            yield return "Hello";
            yield return "Hello";
            yield return "Hello";
        }
    }
}