﻿using System;

namespace _012_yield
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string item in MyCollection.AsEnumerable())
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
    }
}