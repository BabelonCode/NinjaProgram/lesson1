﻿namespace _001_Enumerable
{
    class ListNode
    {
        public int value;
        public ListNode next;

        public override string ToString() => value.ToString();
    }
}