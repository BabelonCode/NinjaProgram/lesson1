﻿namespace _004_Enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            ListNode node1 = new ListNode(10);
            for (int value = 20; value < 60; value+=10)
            {
                node1.Add(value);
            }
        }
    }
}