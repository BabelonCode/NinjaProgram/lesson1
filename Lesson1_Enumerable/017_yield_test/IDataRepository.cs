﻿using System;
using System.Collections.Generic;
using System.Data;

namespace _017_yield_test
{
    public interface IDataRepository
    {
        //IEnumerable<TModel> ExecuteAsEnumerable<TModel>(string query) where TModel : class, new();
        IEnumerable<TModel> ExecuteAsEnumerable<TModel>(string query, Func<IDataRecord, TModel> convertFunc) where TModel : class, new();
    }
}
