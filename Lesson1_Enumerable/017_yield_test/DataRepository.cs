﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace _017_yield_test
{
    class DataRepository : IDataRepository
    {
        public DataRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        private readonly string _connectionString;

        public IEnumerable<TModel> ExecuteAsEnumerable<TModel>(string query, Func<IDataRecord, TModel> mapper)
            where TModel : class, new()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                using (var comand = conn.CreateCommand())
                {
                    comand.CommandType = CommandType.Text;
                    comand.CommandText = query;
                    var reader = comand.ExecuteReader();

                    while (reader.Read())
                        yield return mapper.Invoke(reader);
                }
            }
        }
    }
}
