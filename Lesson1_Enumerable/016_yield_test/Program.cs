﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace _016_yield_test
{
    class Program
    {
        static void Main(string[] args)
        {
            const int count = 10000;

            var sw = Stopwatch.StartNew();
            var list = CreateList(count);
            int sum = SumList(list);

            //var en = CreateEnumerable(count);
            //int sum = Sum(en);
            sw.Stop();

            Console.WriteLine(sw.Elapsed);
            Console.ReadLine();
        }

        static int SumList(List<int> source)
        {
            int sum = 0;
            foreach (int item in source)
                sum += item;
            return sum;
        }

        static int Sum(IEnumerable<int> source)
        {
            int sum = 0;
            foreach (int item in source)
                sum += item;
            return sum;
        }

        static List<int> CreateList(int count)
        {
            //TODO [Artyom Tonoyan]: Need optimization.
            var list = new List<int>();
            for (int i = 0; i < count; i++)
            {
                list.Add(i);
            }
            return list;
        }

        static IEnumerable<int> CreateEnumerable(int count)
        {
            for (int i = 0; i < count; i++)
                yield return i;
        }
    }
}
