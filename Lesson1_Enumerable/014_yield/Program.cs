﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _014_yield
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = CreateRandomList(100);

            List<int> listEvens = list.EvenNumbers();
            List<int> listG = list.GreaterThen(100);
        }

        static List<int> CreateRandomList(int count)
        {
            var list = new List<int>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
                list.Add(rnd.Next(0, 150));
            return list;
        }
    }
}
