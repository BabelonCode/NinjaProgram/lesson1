﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _013_yield
{
    class MyCollection
    {
        public static IEnumerable<string> AsEnumerable()
        {
            //yield return "Hello";
            //yield return "Hello";
            //yield return "Hello";
            //yield return "Hello";
            //yield return "Hello";
            return new ClassAsEnumerable();
        }

        private sealed class ClassAsEnumerable : IEnumerable<string>, IEnumerator<string>
        {
            private int state;
            private string current;

            public string Current
            {
                get { return current; }
            }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            public void Dispose()
            {
                Reset();
            }

            public IEnumerator<string> GetEnumerator()
            {
                state = 0;
                return this;
            }

            public bool MoveNext()
            {
                switch (state)
                {
                    case 0:
                        current = "Hello";
                        state = 1;
                        return true;

                    case 1:
                        state = -1;
                        break;
                }

                return false;
            }

            public void Reset()
            {
                state = -1;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}