﻿using System.Collections;

namespace _006_Enumerable
{
    class ListNode : IEnumerable
    {
        public ListNode(int item)
        {
            value = item;
        }

        public int value;
        public ListNode next;
        private ListNode _cuurentNode;

        public void Add(int item)
        {
            if (_cuurentNode == null)
            {
                next = new ListNode(item);
                _cuurentNode = next;
            }
            else
            {
                _cuurentNode.next = new ListNode(item);
                _cuurentNode = _cuurentNode.next;
            }
        }

        public override string ToString() => value.ToString();

        public IEnumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        private class Enumerator : IEnumerator
        {
            public Enumerator(ListNode root)
            {
                _node = root;
            }

            public object Current { get; private set; }
            private ListNode _node;

            public bool MoveNext()
            {
                if (_node == null)
                    return false;

                Current = _node.value;
                _node = _node.next;
                return true;
            }

            public void Reset()
            {
                _node = null;
            }
        }
    }
}