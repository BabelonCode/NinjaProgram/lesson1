﻿using System.Collections.Generic;

namespace _009_Enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            short[] arr = { 10, 20, 1000, 40 };
            var list = new List<int> { 10, 20, 1000, 40 };
            var listNode = new ListNode<int>(10) { 20, 1000, 40 };

            int sum1 = MyMath.Sum(arr);
            int sum2 = MyMath.Sum(list);
            int sum3 = MyMath.Sum(listNode);
        }
    }
}
