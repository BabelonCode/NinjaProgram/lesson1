﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace _009_ICollection
{
    class ListNode<T> : ICollection<T>
    {
        public ListNode(T item)
        {
            value = item;
        }

        public T value;
        public ListNode<T> next;
        private ListNode<T> _cuurentNode;

        public int Count => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(T item)
        {
            if (_cuurentNode == null)
            {
                next = new ListNode<T>(item);
                _cuurentNode = next;
            }
            else
            {
                _cuurentNode.next = new ListNode<T>(item);
                _cuurentNode = _cuurentNode.next;
            }
        }

        public override string ToString() => value.ToString();

        public IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        private class Enumerator : IEnumerator<T>
        {
            public Enumerator(ListNode<T> root)
            {
                _node = root;
            }

            private ListNode<T> _node;

            public T Current { get; private set; }

            object IEnumerator.Current => Current;

            public bool MoveNext()
            {
                if (_node == null)
                    return false;

                Current = _node.value;
                _node = _node.next;
                return true;
            }

            public void Reset()
            {
                _node = null;
            }

            public void Dispose()
            {
                Reset();
            }
        }
    }
}